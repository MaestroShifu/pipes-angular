import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private nombre:string = "fernando";

  private array:number[] = [1,2,3,4,5,6,7,8,9,10];

  private PI:number = Math.PI;

  private a:number = 0.230;

  private salario:number = 870000.50;

  private heroe:any = {
    nombre: "Logan",
    clave: "Wolveri",
    edad: 500,
    direccion:{
      calle: "Primera",
      casa: "19"
    }
  };

  valorPromesa = new Promise((resolve, reject) =>{
    setTimeout(()=>resolve('LLego la data'), 3500);
  });

  private fecha:Date = new Date();
}
